# FTTM Tweets That Matter (beta)
## What does it do?
It only shows the latest 100 posts from the Facebook feed of the Filipino Tweets that Matter page. 

It also shows realtime stats such as shares and reacts in every post too so you can see how it spreads so fast.
## That's it?
Yeah. Not kind of a big deal. The goal of this project is just to show off my Vue.js/Javascript skills since it's my first time using it.

## Libraries used?
[Vue.js](https://vuejs.org), [Facebook Graph API](https://developers.facebook.com/docs/graph-api/), [Moment.js](https://momentjs.com), and [Tachyons.css](https://tachyons.io). 

## Changelog
- 1.0 beta
  - initial release
  
#### (c) 2017 Ned Palacios