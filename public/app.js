var fttm = new Vue({
  el: "#posts",
  data: {
    posts: []
  },
  mounted() {
    this.loadData();
  },
  watch: {
    posts: function() {
      setTimeout(() => {
        this.loadData();
      }, 5000); 
    }
  },
  methods: {
    loadData() {
      var ACCESS_TOKEN = "221049541695446|mF58htYN4L5Gd0nHdbem-gCkGrI"
      var PAGE_ID = '816893805109455'
      var fields = 'type,full_picture,message,reactions.summary(total_count),shares,created_time'
      var url = 'https://graph.facebook.com/' + PAGE_ID + '/posts?limit=100&fields='+ fields +'&access_token=' + ACCESS_TOKEN
      axios.get(url)
        .then(response => {
          this.posts = response.data['data'];
        })
    },
    reversedText(word) {
      return word.split('').reverse().join('');
    },
    filteredMsg(text) {
      return twemoji.parse(this.reversedText(this.reversedText(text).slice(143)));
    },
    humanizeTime(date){
      var date2 = moment(date);
      var manila = date2.tz('Asia/Manila').format('YYYY-MM-DDTHH:mm:ss');
      return moment(manila, "YYYY-MM-DDTHH:mm:ss").fromNow();
    }
  },
  computed: {
    filteredPosts() {
     var footer = "✨ Submissions are welcome through our twitter page: @officialFTTM. Please read the official guidelines on our pinned post before submitting. ✨";
     return this.posts.filter(post => { 
        if (post.type === 'photo' && post.message !== undefined && post.message.indexOf(footer) !== -1) { return post };
     });
    }
  }
});